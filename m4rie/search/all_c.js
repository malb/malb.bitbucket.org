var searchData=
[
  ['ncols',['ncols',['../structmzd__poly__t.html#a3315a54e105c23f95b03838bd6570379',1,'mzd_poly_t::ncols()'],['../structmzd__slice__t.html#a6e158046f14964e5bcdac5d907a26116',1,'mzd_slice_t::ncols()'],['../structmzed__t.html#a24a031a95d2d1427becb0571b7c15d34',1,'mzed_t::ncols()']]],
  ['newton_5fjohn_2eh',['newton_john.h',['../newton__john_8h.html',1,'']]],
  ['njt_5fmzed_5ffree',['njt_mzed_free',['../newton__john_8h.html#a92e8df4c96751e7b184455eb4af9ceed',1,'newton_john.c']]],
  ['njt_5fmzed_5finit',['njt_mzed_init',['../newton__john_8h.html#a51c118498373dc785d38766f889536e6',1,'newton_john.c']]],
  ['njt_5fmzed_5ft',['njt_mzed_t',['../structnjt__mzed__t.html',1,'']]],
  ['nrows',['nrows',['../structmzd__poly__t.html#adefa5f5a4557a284f5955a1f95c1b935',1,'mzd_poly_t::nrows()'],['../structmzd__slice__t.html#a0413b0a48033a22a2a4c1c05b55ec2b4',1,'mzd_slice_t::nrows()'],['../structmzed__t.html#a1ce48de2fae36b008ba96609c797d58f',1,'mzed_t::nrows()']]]
];
